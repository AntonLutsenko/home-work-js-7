// DOM дает возможность обращаться к обьектам HTML(изменять, добавлять)

const list = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const parentBody = document.body;

function newList (list, parent) {
     let mapList = list.map(function(element) {
          return `<li>${element}</li>`
     })
     parent.insertAdjacentHTML('afterbegin', `<ul>${mapList.join('')}</ul>`)
}

newList(list, parentBody);
